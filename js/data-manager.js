var dataManager = {
  dataUrl: 'data/entries.json',
  //dataUrl: 'http://ouvroir-litt-arts.univ-grenoble-alpes.fr/bibliotheque_observatoire.json',
  entries: [],
  process: function(entries) {
    var processedData = {
      "countries": [],
      "genres": [],
      "authors": [],
      "countryEntries": [],
      "genreEntries": [],
      "authorEntries": []
    };

    for (var i = 0; i < entries.length; i++) {
      var entry = entries[i];
      var country = entry.country;
      var genre = entry.genre;
      var author = entry.author;

      if (processedData.countries.indexOf(country) == -1)
        processedData.countries.push(country);

      if (processedData.genres.indexOf(genre) == -1)
        processedData.genres.push(entry.genre);

      if (processedData.authors.indexOf(author) == -1)
        processedData.authors.push(entry.author);

      if (processedData.genreEntries[genre] === undefined) {
        processedData.genreEntries[genre] = [];
      }
      processedData.genreEntries[genre].push(entry);

      if (processedData.countryEntries[country] === undefined) {
        processedData.countryEntries[country] = [];
      }
      processedData.countryEntries[country].push(entry);

      if (processedData.authorEntries[author] === undefined) {
        processedData.authorEntries[author] = [];
      }
      processedData.authorEntries[author].push(entry);
    }

    return processedData;
  },


  listCountries: function() {
    var countries = this.data.countries;
    for (var i = 0; i < countries.length; i++) {
      var li = document.createElement('li');
      li.setAttribute('class', 'list-group-item country-list-item');
      li.setAttribute('data-country-name', countries[i]);
      li.innerHTML = countries[i];

      var count = this.data.countryEntries[countries[i]].length;
      var badge = this.createBadge(count);
      li.appendChild(badge);

      document.getElementById('country-list').appendChild(li);
    }

    return;
  },

  listAuthors: function() {
    var authors = this.data.authors;
    for (var i = 0; i < authors.length; i++) {
      var li = document.createElement('li');
      li.setAttribute('class', 'list-group-item author-list-item');
      li.setAttribute('data-author-name', authors[i]);
      li.innerHTML = authors[i];

      var count = this.data.authorEntries[authors[i]].length;
      var badge = this.createBadge(count);
      li.appendChild(badge);

      document.getElementById('author-list').appendChild(li);
    }

    return;
  },

  listGenres: function() {
    var genres = this.data.genres;
    for (var i = 0; i < genres.length; i++) {
      var li = document.createElement('li');
      li.setAttribute('class', 'list-group-item genre-list-item');
      li.setAttribute('data-genre-name', genres[i]);
      li.innerHTML = genres[i];

      var count = this.data.genreEntries[genres[i]].length;
      var badge = this.createBadge(count);
      li.appendChild(badge);

      document.getElementById('genre-list').appendChild(li);
    }

    return;
  },

  createBadge: function(count) {
    var badge = document.createElement('span');
    badge.setAttribute('class', 'badge badge-light pull-right');
    badge.innerHTML = count;

    return badge;
  },

  init: function() {
    /*$.ajax({
      url: dataManager.dataUrl,
      type: "GET",
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      //data: JSON.stringify(somejson),
      dataType: "json", //json
      success: function(response) {
        var resp = response; //JSON.parse(response)
        console.log("response " + response);
        alert(resp.status);
      },
      error: function(xhr, status) {
        alert("error");
        console.log("XHR " + xhr);
        console.log("Status " + status);
      }
    });*/
        $.get(dataManager.dataUrl, function(json) {
          dataManager.entries = json.entries;
          var data = dataManager.entries;
          dataManager.data = dataManager.process(data);
          dataManager.listCountries();
          dataManager.listGenres();
          dataManager.listAuthors();
        });

    $(document).on("click", ".country-list-item", dataManager.clickCountry);
    $(document).on("click", ".genre-list-item", dataManager.clickGenre);
    $(document).on("click", ".author-list-item", dataManager.clickAuthor);
    $(document).on("click", "#all-entries", dataManager.clickAll);

    $("input#search").quicksearch("#current-country .list-group-item")
  },

  clickCountry: function() {
    var countryName = this.dataset.countryName;
    var entries = dataManager.data.countryEntries[countryName];

    dataManager.displayEntries(entries, countryName);
  },

  clickGenre: function() {
    var genreName = this.dataset.genreName;
    var entries = dataManager.data.genreEntries[genreName];
    dataManager.displayEntries(entries, genreName);
  },

  clickAuthor: function() {
    var authorName = this.dataset.authorName;
    var entries = dataManager.data.authorEntries[authorName];
    dataManager.displayEntries(entries, authorName);
  },

  clickAll: function() {
    var entries = dataManager.entries;
    dataManager.displayEntries(entries, "Toutes les entrées");
  },

  displayByCountry: function(countryName) {
    var entries = dataManager.data.countryEntries[countryName];
    dataManager.displayEntries(entries, countryName);
  },

  displayEntries: function(entries, subset) {
    $('#search').val('').trigger('keyup');
    var entriesContainer = document.getElementById('current-country');
    entriesContainer.innerHTML = "";
    document.getElementById('subset').innerHTML = subset;

    if (entries !== undefined) {
      for (var i = 0; i < entries.length; i++) {
        var li = this.createEntry(entries[i]);
        document.getElementById('current-country').appendChild(li);
      }
      qs.cache();
    }

  },

  createEntry: function(entry) {
    var li = document.createElement('li');
    li.setAttribute('class', 'list-group-item');

    var genre = document.createElement('span');
    genre.setAttribute('class', 'genre badge badge-secondary');
    genre.innerHTML = entry.genre;
    li.appendChild(genre);

    var title = document.createElement('span');
    title.setAttribute('class', 'title');
    title.innerHTML = entry.title;
    li.appendChild(title);

    var author = document.createElement('span');
    author.setAttribute('class', 'author');
    author.innerHTML = entry.author;
    li.appendChild(author);

    var publication = document.createElement('span');
    publication.setAttribute('class', 'badge badge-info pull-right');
    publication.innerHTML = entry.publication;
    li.appendChild(publication);

    return li;
  },
};

dataManager.init();
var qs = $("input#search").quicksearch("#current-country .list-group-item");

$(document).ready(function() {
  dataManager.clickAll();
});
