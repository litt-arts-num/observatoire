<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Bibliotheca / AL-Maqtaba</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
      crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css" />
  </head>

  <body>
    <nav id="navbar" class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="index.php">
        <i class="fa fa-globe" aria-hidden="true"></i> Bibliotheca / AL-Maqtaba - <i>La bibliothèque de l'Observatoire des littératures francophones
          du Sud</i>
      </a>
      <div class="collapse navbar-collapse">
      </div>
      <div>
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="btn btn-secondary nav-link" href="map.php">
              <i class="fa fa-map " aria-hidden="true"></i> Afficher la carte
            </a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="container">
      <div class="row">


        <div class="col-6">
          <div class="jumbotron">
            <div class="text-justify">
              <i> En ce début de XXI siècle, la création francophone occupe une place très importante dans la vie culturelle du Maghreb. Dans le
                sillage des pères fondateurs (au tournant des indépendances), de la « littérature de l’urgence » (à la fin du XX siècle), de nombreux
                écrivains participent de nos jours au renouvellement des écritures littéraires tant en Algérie qu’au Maroc et qu’en Tunisie. Cet
                effort de création, entre fidélité et invention, s’accompagne de l’émergence et du développement spectaculaire de maisons d’édition
                qui se donnent pour mission de diffuser cette nouvelle littérature auprès d’un public aussi large et diversifié que possible. C’est
                cette nouvelle créativité et cette vitalité éditoriale que les rencontres internationales de Lyon et de Grenoble souhaitent mettre en
                valeur et faire mieux connaître en France. </i>
            </div>
          </div>
        </div>
        <div class="col-6">
          <img onclick="window.location.href='map.php'" style="max-width:100%;cursor: pointer;" class="rounded" src="img/index.gif" />
        </div>

      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <p> Il se définit comme une plateforme collaborative animée par des équipes de recherche basées dans de nombreux pays francophones et
                francophiles du Sud. <br/> Les chercheurs ont pour objectif commun de : <ul>
                  <li>Faire connaître et promouvoir les écrivains et les artistes francophones contemporains publiant dans leurs pays d’origine ;</li>
                  <li>Construire une base de données de productions littéraires et artistiques en collaboration avec les maisons d’édition du Sud ;</li>
                  <li>Stimuler la recherche autour de cette littérature en organisant des manifestations culturelles et des ateliers d’écriture avec
                    les écrivains ;</li>
                  <li>Contribuer au renouvellement du champ francophone en Occident en s’ouvrant sur les cultures et les langues du Sud.</li>
                </ul>
              </p>
              <p> Les membres permanents du projet : <ul>
                  <li>Khadidja Khelladi, université d’Alger</li>
                  <li>Atika Kara, Ecole normale supérieure Bouzaréah d’Alger</li>
                  <li>Lamia Oucherif, Ecole normale supérieure Bouzaréah d’Alger</li>
                  <li>Saïm Voussad, Ecole normale supérieure Bouzaréah d’Alger</li>
                  <li>Fatima Medjad, université d’Oran 2</li>
                  <li>Sonia Zlitni-Fitouri, université de Tunis</li>
                  <li>Jalel El-Gharbi, université de la Manouba, Tunis</li>
                  <li>Mohamed Lehdehda, université de Moulay Ismaïl de Meknès</li>
                  <li>Touriya Fili-Tulon, université des Lumières Lyon2</li>
                  <li>Sylvie Brodziak, université de Cergy-Pontoise</li>
                  <li>Claude Coste, université de Cergy-Pontoise</li>
                </ul>
              </p>
            </div>
          </div>
        </div>
      </div>
      <nav id="footer" class="navbar navbar-expand-lg navbar-light bg-light">
        <img src="img/logos/Logo_UGA.png" class="logo inline" alt="Logo UGA" />
        <img src="img/logos/Logo_Litt&Arts.png" class="logo list-inline" alt="Logo Litt&Arts" />
        <img src="img/logos/Logo_LFEF.jpg" class="logo list-inline" alt="Logo Langue Française & Expression Francophone" />
        <img src="img/logos/12.png" class="logo list-inline" alt="Logo ???" />
        <img src="img/logos/3.jpg" class="logo list-inline" alt="Logo ???" />
        <img src="img/logos/4.jpg" class="logo list-inline" alt="Logo ???" />
        <img src="img/logos/6.png" class="logo list-inline" alt="Logo ???" />
        <img src="img/logos/7.png" class="logo list-inline" alt="Logo ???" />
        <img src="img/logos/8.jpg" class="logo list-inline" alt="Logo ???" />
        <img src="img/logos/9.png" class="logo list-inline" alt="Logo ???" />
        <img src="img/logos/11.png" class="logo list-inline" alt="Logo ???" />
        <img src="img/logos/10.jpg" class="logo list-inline" alt="Logo ???" />
      </nav>
    </div> <!-- <div class="container-fluid"> -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
      crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
      crossorigin="anonymous"></script>
  </body>

</html>
