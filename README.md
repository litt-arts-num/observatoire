# Installation

# Production des données au format json à partir de Drupal

## Principe
Les données bibliographiques au format json sont produites directement à partir de Drupal où les participants au projet disposent d'un formulaire permettant de créer et modifier des fiches géo-biliographiques.

## Marche à suivre
Voici la marche à suivre sous Drupal pour générer le json `entries.json` :

- Depuis le tableau de bord de Drupal (donc après connexion), aller dans "Views" et choisir "Add new view"

![Création de la vue](doc/drupal_create_view.png "Création de la vue drupal")

- Configurer la view avec notamment les éléments suivants :

  - Concernant le format : JSON data document

    - Root object name : entries
    - Top-level child object : _vide_
    - Field output : Normal
    - Plaintext output : _checked_
    - Remove newlines : _checked_
    - JSON data format : Simple
    - JSONP prefix : _vide_
    - Content-Type : application/javascript
    - Pretty print : _checked_

  - Fields :

    - Content revision: Titre (label: title ; place a colon after the label)
    - Contenu: Auteur (author)
    - Contenu: Date (année) (year)

      - Formater : _date et heure_
      - Choose how users view dates and times: _Année seulement_

    - Contenu: Données de publication (publication)

    - Contenu: Genre (genre)
    - Contenu: Pays (country)

  - Pager :

    - Use pager: Display all items | All items
    - More link: Non

![Paramétrage de la vue](doc/drupal_config_view.png "Paramétrage de la vue drupal")

## Fichiers utiles
- [Export de la view drupal](doc/drupal_view.txt)

- [Voir le json produit](data/entries.json)

