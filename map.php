<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Bibliotheca / AL-Maqtaba</title>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css" />
  <script src="js/d3.min.js"></script>
  <script src="js/topojson.v1.min.js"></script>
</head>

<?php
      $site = "http://ouvroir-litt-arts.univ-grenoble-alpes.fr/bibliotheque_observatoire.json";
      $file = @fopen($site, 'r');
      if ($file) {
          if (!@copy($site, 'data/entries.json')) {
              $errors= error_get_last();
              echo "COPY ERROR: ".$errors['type'];
              echo "<br />\n".$errors['message'];
          } else {
              echo "";
          }
      } else {
          echo "<!-- Attention : la source JSON n'est donnée n'est pas accessible ! -->";
      }
?>

<body>
  <nav id="navbar" class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="index.php">
      <i class="fa fa-globe" aria-hidden="true"></i>
      Bibliotheca / AL-Maqtaba - <i>La bibliothèque de l'Observatoire des littératures francophones du Sud</i>
    </a>
    <div class="collapse navbar-collapse">
    </div>
    <div>
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link btn btn-secondary" href="index.php">
              <i class="fa fa-home" aria-hidden="true"></i>
              Présentation du projet
          </a>
        </li>
      </ul>
    </div>
  </nav>
  <!--<p>  <a href="http://techslides.com/d3-map-starter-kit/">back to article</a></p>  -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-3">
        <div id="all-entries" class="card">
          <h5 class="card-header">
              <i class="fa fa-list" aria-hidden="true"></i>
              Toutes les entrées bibliographiques
            </h5>
        </div>

        <hr />

        <div class="card">
          <h5 class="card-header">
              <i class="fa fa-flag" aria-hidden="true"></i>
              Pays
            </h5>
          <ul id="country-list" class="list-group list-group-flush">
          </ul>
        </div>
        <hr />
        <div class="card">
          <h5 class="card-header">
              <i class="fa fa-sticky-note" aria-hidden="true"></i>
              Genres
            </h5>
          <ul id="genre-list" class="list-group list-group-flush">
          </ul>
        </div>

      </div>
      <div class="col-6">
        <div id="container"></div>
        <hr/>
        <div id="subset-container" class="form-row">
          <div class="col-8 ">
            <h3 id="subset"></h3>
          </div>
          <div class="col">
            <div class="input-group">
              <span class="input-group-addon" id="btnGroupAddon">
                     <i class="fa fa-search" aria-hidden="true"></i>
                   </span>
              <input class="form-control" type="text" id="search" />
            </div>
          </div>
        </div>
        <div class="card">
          <ul id="current-country" class="list-group">
          </ul>
        </div>
      </div>
      <div class="col-3">
        <div class="card">
          <h5 class="card-header">
              <i class="fa fa-users" aria-hidden="true"></i>
              Auteurs
            </h5>
          <ul id="author-list" class="list-group list-group-flush">
          </ul>
        </div>
      </div>
    </div>
    <nav id="footer" class="navbar navbar-expand-lg navbar-light bg-light">
                  <img src="img/logos/Logo_UGA.png" class="logo inline" alt="Logo UGA" />
                  <img src="img/logos/Logo_Litt&Arts.png" class="logo list-inline" alt="Logo Litt&Arts" />
                  <img src="img/logos/Logo_LFEF.jpg" class="logo list-inline" alt="Logo Langue Française & Expression Francophone"/>
                  <img src="img/logos/12.png" class="logo list-inline" alt="Logo ???"/>
                  <img src="img/logos/3.jpg" class="logo list-inline" alt="Logo ???"/>
                  <img src="img/logos/4.jpg" class="logo list-inline" alt="Logo ???"/>
                  <img src="img/logos/6.png" class="logo list-inline" alt="Logo ???"/>
                  <img src="img/logos/7.png" class="logo list-inline" alt="Logo ???"/>
                  <img src="img/logos/8.jpg" class="logo list-inline" alt="Logo ???"/>
                  <img src="img/logos/9.png" class="logo list-inline" alt="Logo ???"/>
                  <img src="img/logos/11.png" class="logo list-inline" alt="Logo ???"/>
                  <img src="img/logos/10.jpg" class="logo list-inline" alt="Logo ???"/>
  </nav>
    </div>
</div> <!-- <div class="container-fluid"> -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <script src="js/jquery.quicksearch.min.js"></script>
  <script src="js/data-manager.js"></script>
  <script src="js/map-manager.js"></script>
</body>

</html>
